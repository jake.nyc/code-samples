# provider

provider "digitalocean" {
  token = "YOUR_API_TOKEN_GOES_HERE"
}

# variables

variable "color" {
  type        = string
  description = "Which set of servers should be promoted."
  validation {
    condition     = var.color == "blue" || var.color == "green"
    error_message = "The color must be blue or green."
  }
}

variable "cautious" {
  type        = bool
  default     = true
  description = "Whether to keep the demoted server set running."
}

# application servers

locals {
  num_servers = 1
}

data "digitalocean_droplet_snapshot" "blue" {
  name_regex  = "^blue-"
  region      = "nyc3"
  most_recent = true
}

resource "digitalocean_droplet" "blue" {
  name       = "blue-${count.index + 1}"
  count      = var.color == "blue" || var.cautious ? local.num_servers : 0
  image      = data.digitalocean_droplet_snapshot.blue.id
  region     = "nyc3"
  size       = "s-1vcpu-1gb"
  monitoring = true
}

data "digitalocean_droplet_snapshot" "green" {
  name_regex  = "^green-"
  region      = "nyc3"
  most_recent = true
}

resource "digitalocean_droplet" "green" {
  name       = "green-${count.index + 1}"
  count      = var.color == "green" || var.cautious ? local.num_servers : 0
  image      = data.digitalocean_droplet_snapshot.green.id
  region     = "nyc3"
  size       = "s-1vcpu-1gb"
  monitoring = true
}

# load balancer

locals {
  promoted_servers = var.color == "blue" ? digitalocean_droplet.blue : digitalocean_droplet.green
  demoted_servers  = var.color == "blue" ? digitalocean_droplet.green : digitalocean_droplet.blue
}

data "digitalocean_droplet_snapshot" "balancer" {
  name_regex  = "^balancer-"
  region      = "nyc3"
  most_recent = true
}

resource "digitalocean_droplet" "balancer" {
  name       = "balancer"
  image      = data.digitalocean_droplet_snapshot.balancer.id
  region     = "nyc3"
  size       = "s-1vcpu-1gb"
  monitoring = true
}


resource "null_resource" "provision_balancer" {
  triggers = {
    balancer = digitalocean_droplet.balancer.id
    servers  = "${join(",", concat(local.promoted_servers.*.id, local.demoted_servers.*.id))}"
  }

  connection {
    type = "ssh"
    host = digitalocean_droplet.balancer.ipv4_address
  }

  provisioner "file" {
    content = templatefile("./nginx.conf", {
      promoted_servers = local.promoted_servers.*.ipv4_address
      demoted_servers  = local.demoted_servers.*.ipv4_address
    })
    destination = "/tmp/songrender.com"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/songrender.com /etc/nginx/sites-available/songrender.com",
      "sudo ln -sf /etc/nginx/sites-available/songrender.com /etc/nginx/sites-enabled/songrender.com",
      "sudo systemctl reload nginx"
    ]
  }
}
